'use strict';

// native
//var util = require('util');
var fs = require('fs');
var path = require('path');
var http = require('http');
var exec = require('child_process').exec;

// 3rd party
var mongo;// = require('expoj-mongo');
var returnCodes;// = require('laposte').returnCodes;
var logger;// = log4js.getLogger('launch');

/** @type {string} */
var LOAD_TEST_CLIENT_INFO_COLLECTION;// = 'sbm_clients';
var LOAD_TEST_PAST_MONITORED_TESTS;// = 'sbm_monitoringResults';
var LOAD_TEST_SERVER_NODES;// = 'sbm_hosts';


var measureDelay = 60 * 1000; // 1 minute
var defaultDB;

/** @type {string} */
var archiveFolderPath;

/**
 * Setup necessary variables for the plugin
 * @param {Log4jsLogger} Logger
 * @param DefaultDB
 * @param ReturnCodes
 * @param Mongo
 * @param {string} clientInfoCollection
 * @param {string} pastMonitoredTestsCollection
 * @param {string} serverNodesCollection
 * @param {string} archivePath
 */
function setup(Logger, DefaultDB, ReturnCodes, Mongo, clientInfoCollection, pastMonitoredTestsCollection, serverNodesCollection, archivePath) {
    logger = Logger;
    defaultDB = DefaultDB;
    returnCodes = ReturnCodes;
    mongo = Mongo;
    LOAD_TEST_CLIENT_INFO_COLLECTION = clientInfoCollection;
    LOAD_TEST_PAST_MONITORED_TESTS = pastMonitoredTestsCollection;
    LOAD_TEST_SERVER_NODES = serverNodesCollection;

    archiveFolderPath = archivePath;
}

// local variables
var runningSources = [];
var matchIdWithAddressAndPort = {};
var lastTestId = null;
var runningServers = [];

/**
 * Make a get HTTP request on address:port/path
 * @param {string} address
 * @param {string|number} port
 * @param {string} path
 * @param {function (Error, string)} callback
 */
function callWget(address, port, path, callback) {
    if (path[0] !== '/') {
        path = '/' + path;
    }
    var options = {
        hostname: address,
        port: port,
        path: path,
        method: 'GET'
    };

    var req = http.request(options, function (response) {
        var str = '';

        //another chunk of data has been received, so append it to `str`
        response.on('data', function (chunk) {
            str += chunk;
        });

        //the whole response has been received, so we just print it out here
        response.on('end', function () {
            callback(undefined, str);
        });
    });

	req.setTimeout(2000, function(err) {
		if (err && err.code === "ECONNRESET") {
			logger.warn("callWget timeout occurs");
			callback(err);
		}
		callback(returnCodes.NOT_FOUND);
	});
	
    req.on('error', function (error) {
        callback(error);
    });

    req.end();
}

function getTestResult(query, callback) {
    if (!query.id) {
        callback(returnCodes.BAD_REQUEST);
        return;
    }

    defaultDB.collection(LOAD_TEST_PAST_MONITORED_TESTS).find({ _id: parseInt(query.id, 10)}).next(function (err, result) {
        if (err) {
            callback(returnCodes.INTERNAL_ERROR);
            return;
        }

        callback(returnCodes.OK, result);
    });
}

function wrapWget(localAddress, path, callback) {
    callWget(localAddress, 1350, path, function (error, str) {
        callback(path, str);
    });
}

function fetchAndStoreUnit(hostId, localAddress, lastTestId) {
    var objectToPush = {};
    var apiStrings = ['/hw/cpu', '/hw/memory', '/hw/disk'];
    var decreasingCounter = apiStrings.length;
    var i;

    function done(key, value) {
        objectToPush[key] = value;
        decreasingCounter = decreasingCounter - 1;
        if (decreasingCounter === 0) {
            var pushObject = {};
            pushObject[hostId] = objectToPush;

            defaultDB.collection(LOAD_TEST_PAST_MONITORED_TESTS).findOneAndUpdate(
                { _id: lastTestId },
                { $push: pushObject },
                function (err) {
                    if (err) {
                        logger.error(err);
                    }
                }
            );
        }
    }

    for (i = 0; i < apiStrings.length; i = i + 1) {
        wrapWget(localAddress, apiStrings[i], done);
    }
}

function fetchAndAddToTest(sourceId) {
    var i;
    if (lastTestId === null) {
        throw new Error('The lastTestId was null.');
    }

    for (i = 0; i < runningServers.length; i = i + 1) {
        fetchAndStoreUnit(runningServers[i].id, runningServers[i].localAddress, lastTestId);
    }
}

function isAlive(sourceId, cb) {
    callWget(matchIdWithAddressAndPort[sourceId][0], matchIdWithAddressAndPort[sourceId][1], 'version', function (error, str) {
        if (error) {
            cb(false);
            return;
        }

        cb(true);
    });
}

function waitForSourceToBeAlive(sourceId, retries, cb) {
    isAlive(sourceId, function (alive) {
        if (alive) {
            cb(true);
            return;
        }
        if (retries === 0) {
            cb(false);
            return;
        }
        setTimeout(function () {
            waitForSourceToBeAlive(sourceId, retries - 1, cb);
        }, 3000);
    });
}

function retrieveAddressAndPort(sourceId, cb) {
    defaultDB.collection(LOAD_TEST_CLIENT_INFO_COLLECTION).find({_id: sourceId}, {_id: 0}).next(
        function (err, server) {
            if (err) {
                cb(err);
                return;
            }
            // launch bash deployment script
            matchIdWithAddressAndPort[sourceId] = [server.address, server.port];
            cb();
        }
    );
}

function createNewTestIfRequired(cb) {
    if (runningSources.length !== 1) {
        cb();
        return;
    }

    defaultDB.collection(LOAD_TEST_SERVER_NODES).find({}).toArray(function (err, result) {
        var i;
        var hosts = {};
        runningServers = [];
        for (i = 0; i < result.length; i = i + 1) {
            hosts[result[i]._id] = [];
            runningServers.push({
                id: result[i]._id,
                localAddress: result[i].localAddress,
                dns: result[i].dns
            });
        }
        lastTestId = Date.now();
        hosts._id = lastTestId;

        defaultDB.collection(LOAD_TEST_PAST_MONITORED_TESTS).insertOne(hosts, function (err) {
            if (err) {
                cb(err);
                return;
            }

            cb();
        });
    });
}

function monitorMainLoop(sourceId) {
    var interval = setInterval(function () {
        isAlive(sourceId, function (alive) {
            if (alive) {
                fetchAndAddToTest(sourceId);
            } else {
                runningSources.splice(runningSources.indexOf(sourceId), 1);
                clearInterval(interval);
            }
        });
    }, measureDelay);
}

function startMonitoringFrom(sourceId) {
    retrieveAddressAndPort(sourceId, function (err) {
        if (err) {
            logger.error('Failed to match the source ID. ( ' + err + ')');
            return;
        }
        waitForSourceToBeAlive(sourceId, 10, function (alive) {
            if (!alive) {
                return;
            }

            runningSources.push(sourceId);
            createNewTestIfRequired(function (err) {
                if (err) {
                    logger.error('Failed to create a new test entry. ( ' + err + ')');
                    return;
                }
                monitorMainLoop(sourceId);
            });
        });
    });
}

function killTest(request, callback) {
    if (!request) {
        callback(returnCodes.BAD_REQUEST);
        return;
    }
    var data = request.data;
    if (!data.serverId) {
        callback(returnCodes.BAD_REQUEST);
        return;
    }

    //Convert the _id to an ObjectID if necessary
    if (data.serverId.indexOf('ObjectID(') === 0) {
        data.serverId = mongo.ObjectID(data.serverId.substring(9, data.serverId.length - 1));
    }

    defaultDB.collection(LOAD_TEST_CLIENT_INFO_COLLECTION).find({_id: data.serverId}, {_id: 0}).next(
        /**
         * @param {MongoError|undefined} err
         * @param {{}} server
         * @param {string} server.address
         * @param {number} server.port
         */
        function (err, server) {
            if (err) {
                logger.error('Failed retrieving system settings');
                callback(returnCodes.INTERNAL_ERROR);
                return;
            }

            // launch bash deployment script
            callWget(server.address, server.port, 'kill', function (error, string) {
                if (error) {
                    logger.error('Failed to kill the load test client (' + error + ')');
                    callback(returnCodes.OK);
                    return;
                }
                callback(returnCodes.OK);
            });
        }
    );
}

function launchTests(request, callback) {
    if (!request) {
        callback(returnCodes.BAD_REQUEST);
        return;
    }
    var data = request.data;
    if (!data.serverId) {
        callback(returnCodes.BAD_REQUEST);
        return;
    }

    //Convert the _id to an ObjectID if necessary
    if (data.serverId.indexOf('ObjectID(') === 0) {
        data.serverId = mongo.ObjectID(data.serverId.substring(9, data.serverId.length - 1));
    }

    startMonitoringFrom(data.serverId);

    defaultDB.collection(LOAD_TEST_CLIENT_INFO_COLLECTION).find({_id: data.serverId}, {_id: 0}).next(
        /**
         * @param {MongoError|undefined} err
         * @param {{}} server
         * @param {string} server.address
         * @param {number} server.port
         * @param {string} server.version
         * @param {string} server.configuration
         */
        function (err, server) {
            if (err) {
                logger.error('Failed retrieving system settings');
                callback(returnCodes.INTERNAL_ERROR);
                return;
            }

            // launch bash deployment script
            exec('bash load-tests-launcher/deploy.sh "' + server.address + '" "' + server.port + '" "' + server.version + '" "' + server.configuration + '"', function (error, stdout, stderr) {
                if (error) {
                    callback(returnCodes.OK, JSON.stringify({stdout: stdout, stderr: error.toString()}));
                    return;
                }
                callback(returnCodes.OK, JSON.stringify({stdout: stdout, stderr: stderr}));
            });
        }
    );
}


function getTestStatus(request, callback) {
    if (!request) {
        callback(returnCodes.BAD_REQUEST);
        return;
    }
    var data = request.data;
    if (!data.serverId) {
        callback(returnCodes.BAD_REQUEST);
        return;
    }

    //Convert the _id to an ObjectID if necessary
    if (data.serverId.indexOf('ObjectID(') === 0) {
        data.serverId = mongo.ObjectID(data.serverId.substring(9, data.serverId.length - 1));
    }

    defaultDB.collection(LOAD_TEST_CLIENT_INFO_COLLECTION).find({_id: data.serverId}, {_id: 0}).next(
        /**
         * @param {MongoError|undefined} err
         * @param {{}} server
         * @param {string} server.address
         * @param {number} server.port
         */
        function (err, server) {
            if (err) {
                logger.error('Failed retrieving system settings');
                callback(returnCodes.INTERNAL_ERROR);
                return;
            }

            var scopedCounter = 0;
            var results = { progress: null, status: null, errors: null, version: null };

            var localWrapWget = function (key, error, string) {
                if (scopedCounter === -1) {
                    return;
                }

                if (error) {
                    callback(returnCodes.NOT_FOUND);
                    scopedCounter = -1;
                    return;
                }

                results[key] = string;
                scopedCounter = scopedCounter + 1;
                if (scopedCounter === 4) {
                    callback(returnCodes.OK, results);
                }
            };

            // retrieve status
            callWget(server.address, server.port, 'progress', function (error, string) {
                localWrapWget('progress', error, string);
            });
            callWget(server.address, server.port, 'status', function (error, string) {
                localWrapWget('status', error, string);
            });
            callWget(server.address, server.port, 'errors', function (error, string) {
                localWrapWget('errors', error, string);
            });
            callWget(server.address, server.port, 'version', function (error, string) {
                localWrapWget('version', error, string);
            });
        }
    );
}

function getTestConfigurationList(data, callback) {
    fs.readdir(archiveFolderPath, function (err, files) {
        if (err) {
            logger.error('Failed to read directory. (' + err + ')');
            callback(returnCodes.OK);
            return;
        }

        var res = {};
        var i;
        var keyValue;
        for (i = 0; i < files.length; i = i + 1) {
            if (files[i].substr(0, 2) === 'c_') {
                keyValue = files[i].substring(2, files[i].length - 5);
                res[keyValue] = keyValue;
            }
        }
        callback(returnCodes.OK, JSON.stringify(res));
    });
}

function getTestVersions(data, callback) {
    fs.readdir(archiveFolderPath, function (err, files) {
        if (err) {
            logger.error('Failed to read directory. (' + err + ')');
            callback(returnCodes.OK);
            return;
        }

        var res = [];
        var toReturn = {};
        var i;
        var match;
        for (i = 0; i < files.length; i = i + 1) {
            if (files[i].substr(0, 8) === 'publish.') {
                match = files[i].match(/publish\.([0-9]*\.[0-9]*\.[0-9]*)\.(tar\.gz|zip)/);
                if (match && match[1]) {
                    res.push(match[1]);
                }
            }
        }

        res.sort(function (a, b) {
            var spA = a.split('.');
            var spB = b.split('.');
            var superior = parseInt(spA[0], 10) < parseInt(spB[0], 10) ||
                parseInt(spA[1], 10) < parseInt(spB[1], 10) ||
                parseInt(spA[2], 10) < parseInt(spB[2], 10);
            if (superior) {
                return 1;
            }
            return -1;
        });

        for (i = 0; i < res.length; i = i + 1) {
            toReturn[res[i]] = res[i];
        }
        callback(returnCodes.OK, JSON.stringify(toReturn));
    });
}

function uploadArchive(request, callback) {
    var match = request.data.files.files.name.match(/publish\.([0-9]*\.[0-9]*\.[0-9]*)\.(tar\.gz|zip)/);
    if (!match || match.length !== 3) {
        callback(returnCodes.NOT_ACCEPTABLE, 'Invalid file name.');
        return;
    }

    var version = match[1];
    fs.readdir(archiveFolderPath, function (err, files) {
        if (err) {
            fs.mkdirSync(archiveFolderPath);
        } else {
            var i;
            for (i = 0; i < files.length; i = i + 1) {
                if (files[i].substr(0, 8) === 'publish.') {
                    match = files[i].match(/publish\.([0-9]*\.[0-9]*\.[0-9]*)\.(tar\.gz|zip)/);
                    if (match && version === match[1]) {
                        callback(returnCodes.NOT_ACCEPTABLE, 'There is already a file with the version ' + match[1]);
                        return;
                    }
                }
            }
        }

        fs.rename(request.data.files.files.path, archiveFolderPath + request.data.files.files.name, function (err) {
            if (err) {
                callback(returnCodes.INTERNAL_ERROR, err);
                return;
            }
            callback(returnCodes.OK);
        });
    });
}

module.exports = {
    setup: setup,
    plugins: {
        launchTests: {
            method: 'POST',
            callback: launchTests,
            anonymous: true,
            contentType: 'application/json'
        },
        killTest: {
            method: 'POST',
            callback: killTest,
            anonymous: true,
            contentType: 'application/json'
        },
        getTestResult: {
            method: 'GET',
            callback: getTestResult,
            anonymous: true,
            contentType: 'application/json'
        },
        getTestStatus: {
            method: 'POST',
            callback: getTestStatus,
            anonymous: true,
            contentType: 'application/json'
        },
        getTestConfigurationList: {
            method: 'GET',
            callback: getTestConfigurationList,
            anonymous: true,
            contentType: 'application/json'
        },
        getTestVersions: {
            method: 'GET',
            callback: getTestVersions,
            anonymous: true,
            contentType: 'application/json'
        },
        uploadArchive: {
            method: 'POST',
            callback: uploadArchive,
            anonymous: true,
            inContentType: 'multipart/form-data',
            outContentType: 'text/plain'
        }
    }
};
