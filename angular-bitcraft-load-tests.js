(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/**
 * Created by richard on 15/02/17.
 */
'use strict';

require('./load-tests.module.js');
require('./test-basic-result');
require('./test-kill-global');
require('./test-launch-all');
require('./test-launcher');
require('./test-launcher-global');
require('./upload-archive');

},{"./load-tests.module.js":2,"./test-basic-result":3,"./test-kill-global":6,"./test-launch-all":9,"./test-launcher":15,"./test-launcher-global":12,"./upload-archive":18}],2:[function(require,module,exports){
/*global angular*/
'use strict';

angular.module('bitcraft-load-tests', [
    'test-basic-result',
    'test-kill-global',
    'test-launch-all',
    'test-launcher',
    'test-launcher-global',
    'upload-archive'
]);

},{}],3:[function(require,module,exports){
'use strict';
require('./test-basic-result.module.js');
require('./test-basic-result.component.js');

},{"./test-basic-result.component.js":4,"./test-basic-result.module.js":5}],4:[function(require,module,exports){
/*global angular*/
'use strict';

var template = '' +
'<style>' +
'    thead > tr > th {' +
'        text-align: center;' +
'    }' +
'</style>' +
'<table class="table table-bordered table-hover">' +
'    <thead>' +
'        <tr>' +
'            <th></th>' +
'            <th colspan="3" ng-repeat="cell in headHostnames">{{cell}}</th>' +
'        </tr>' +
'        <tr>' +
'            <th></th>' +
'            <th ng-repeat="cell in headTitle">{{cell}}</th>' +
'        </tr>' +
'    </thead>' +
'    <tbody>' +
'        <tr ng-repeat="line in lines">' +
'            <th>{{line.header}}</th>' +
'            <td ng-repeat="cell in line.data track by $index" style="white-space: pre;">{{cell}}</td>' +
'        </tr>' +
'    </tbody>' +
'</table>' +'';


//noinspection JSUnresolvedFunction
angular.module('test-basic-result').
    component('testBasicResult', {
        template: template,
        controller: ['$scope', '$stateParams', 'Notification', 'RestHttp',
            function ($scope, $stateParams, Notification, RestHttp) {
                this.$onInit = function () {
                    $scope.hello = 'hello';
                    $scope.lines = [];
                    $scope.headHostnames = [];
                    $scope.headTitle = [];
                    RestHttp.restGet('getTestResult', {id: $stateParams.id}).then(function (data) {
                        console.log(data);
                        var key;
                        var keys = [];
                        var startedAt = data._id;
                        var i, j;
                        var done = false;

                        for (key in data) {
                            if (data.hasOwnProperty(key) && key !== '_id') {
                                keys.push(key);
                                $scope.headHostnames.push(key);
                                $scope.headTitle.push('cpu');
                                $scope.headTitle.push('memory');
                                $scope.headTitle.push('disk');

                                if (!done) {
                                    done = true;
                                    for (i = 0; i < data[key].length; i = i + 1, startedAt = startedAt + 60 * 1000) {
                                        $scope.lines.push({header: moment(new Date(startedAt)).format('MM/DD/YYYY hh:mm a'), data: []});
                                    }
                                }
                            }
                        }

                        for (i = 0; i < data[keys[0]].length; i = i + 1) {
                            for (j = 0; j < keys.length; j = j + 1) {
                                $scope.lines[i].data.push(data[keys[j]][i]['/hw/cpu']);
                                $scope.lines[i].data.push(data[keys[j]][i]['/hw/memory']);
                                $scope.lines[i].data.push(data[keys[j]][i]['/hw/disk']);
                            }
                        }

                        $scope.hello = 'hell done';
                    }, function (resp) {
                        Notification.error({
                            message: 'Failed to get the test results (' + resp.statusText + ')'
                        });
                    });
                };
            }],
        bindings: {
            channel: '@'
        }
    });

},{}],5:[function(require,module,exports){
/*global angular*/
'use strict';

angular.module('test-basic-result', []);

},{}],6:[function(require,module,exports){
'use strict';
require('./test-kill-global.module.js');
require('./test-kill-global.component.js');

},{"./test-kill-global.component.js":7,"./test-kill-global.module.js":8}],7:[function(require,module,exports){
/*global angular*/
'use strict';

var template = '' +
'<button class="btn btn-default btn-block" ng-click="$ctrl.killTest()">Kill test</button>' +
'';


//noinspection JSUnresolvedFunction
angular.module('test-kill-global').
    component('testKillGlobal', {
        template: template,
        controller: ['$scope', '$stateParams', 'Notification', 'RestHttp',
            function ($scope, $stateParams, Notification, RestHttp) {
                var self = this;

                self.killTest = function () {
                    RestHttp.restPost('killTest', { serverId: self.id }).then(function () {
                        Notification.success(
                            {message: 'Sent kill command.'}
                        );
                    }, function () {
                        Notification.error(
                            {message: 'Failed to send the command.' + ' (' + resp.statusText + ')'}
                        );
                    });
                };
            }],
        bindings: {
            channel: '@',
            id: '@'
        }
    });

},{}],8:[function(require,module,exports){
/*global angular*/
'use strict';

angular.module('test-kill-global', []);

},{}],9:[function(require,module,exports){
'use strict';
require('./test-launch-all.module.js');
require('./test-launch-all.component.js');

},{"./test-launch-all.component.js":10,"./test-launch-all.module.js":11}],10:[function(require,module,exports){
/*global angular, $*/
'use strict';

var template = '' +
'<hr>' +
'<div class="clearfix">' +
'    <div class="col-xs-6">' +
'        <button class="btn btn-default btn-block" ng-click="$ctrl.startTests()">Launch all tests</button>' +
'    </div>' +
'    <div class="col-xs-6">' +
'        <button class="btn btn-default btn-block" ng-click="$ctrl.killTests()">Kill all tests</button>' +
'    </div>' +
'</div>' +'';


//noinspection JSUnresolvedFunction
angular.module('test-launch-all').
    component('testLaunchAll', {
        template: template,
        controller: ['$timeout', 'Notification',
            function ($timeout, Notification) {
                var self = this;

                this.startTests = function () {
                    $timeout(function () {
                        $('test-launcher-global button').click();
                    });
                };

                this.killTests = function () {
                    $timeout(function () {
                        $('test-kill-global button').click();
                    });
                };
            }],
        bindings: {
            channel: '@'
        }
    });

},{}],11:[function(require,module,exports){
/*global angular*/
'use strict';

angular.module('test-launch-all', []);

},{}],12:[function(require,module,exports){
'use strict';
require('./test-launcher-global.module.js');
require('./test-launcher-global.component.js');

},{"./test-launcher-global.component.js":13,"./test-launcher-global.module.js":14}],13:[function(require,module,exports){
/*global angular*/
'use strict';

var template = '' +
'<button ng-if="!progress || maxRetry === 0 || progress === \'Failed\'" ng-disabled="maxRetry !== 0" class="btn btn-default btn-block" ng-click="$ctrl.launchTests()">Launch tests</button>' +
'<div ng-if="progress">' +
'    <div>Version: {{version}}</div>' +
'    <div>Progress: {{progress}}</div>' +
'    <div ng-if="!hasError">Errors: 0</div>' +
'    <div ng-if="hasError">' +
'        <div ng-if="progress !== \'Failed\'">' +
'            <div ng-show="collapsed">Errors: {{errors.length}}</div>' +
'            <button ng-if="collapsed" ng-click="$ctrl.collapse()" class="btn btn-default btn-block">Expand</button>' +
'            <button ng-if="!collapsed" ng-click="$ctrl.collapse()" class="btn btn-default btn-block" style="margin-bottom: 5px">Collapse</button>' +
'            <table ng-show="!collapsed" class="table table-bordered table-hover" style="margin-bottom: 0px">' +
'                <thead>' +
'                <tr>' +
'                    <th>Scenario</th>' +
'                    <th>Player</th>' +
'                    <th>Message</th>' +
'                </tr>' +
'                </thead>' +
'                <tbody>' +
'                    <tr ng-repeat="error in errors">' +
'                        <td>{{error.scenario}}</td>' +
'                        <td>{{error.player}}</td>' +
'                        <td>{{error.message}}</td>' +
'                    </tr>' +
'                </tbody>' +
'            </table>' +
'        </div>' +
'        <div ng-if="progress === \'Failed\'">' +
'            <div ng-repeat="error in errors">' +
'                <div>{{error.scenario}}: {{error.message}}</div>' +
'            </div>' +
'        </div>' +
'    </div>' +
'</div>' +'';


//noinspection JSUnresolvedFunction
angular.module('test-launcher-global').
    component('testLauncherGlobal', {
        template: template,
        controller: ['$scope', '$stateParams', 'Notification', 'RestHttp',
            function ($scope, $stateParams, Notification, RestHttp) {
                var self = this;

                var formatErrors = function (errors) {
                    var errorArray = errors.split('\r');
                    var resArray = [];
                    var i, len;
                    var message, scenario, player, index;
                    for (i = 0, len = errorArray.length; i < len; i = i + 1) {
                        message = errorArray[i];
                        if (message !== '') { // skip empty lines
                            scenario = '';
                            player = '';
                            if (message.substring(0, 3) === '[Sc') {
                                index = message.indexOf(']') + 1;
                                scenario = message.substring(0, index);
                                scenario = scenario.substring(11, scenario.length - 1);
                                message = message.substring(index);
                                while (message.charAt(0) === ' ') {
                                    message = message.substr(1);
                                }
                            }
                            if (message.substring(0, 3) === '[Pl') {
                                index = message.indexOf(']') + 1;
                                player = message.substring(0, index);
                                player = player.substring(9, player.length - 1);
                                message = message.substring(index);
                                while (message.charAt(0) === ' ') {
                                    message = message.substr(1);
                                }
                            }
                            resArray.push({
                                message: message,
                                player: player,
                                scenario: scenario
                            });
                        }
                    }
                    return resArray;
                };

                var refreshStatus = function () {
                    RestHttp.restPost('getTestStatus', { serverId: self.id }).then(function (data) {
                        console.log(data);
                        $scope.version = data.version;
                        $scope.maxRetry = 1;
                        $scope.progress = data.progress;
                        $scope.hasError = (data.errors !== 'No errors.');
                        if ($scope.hasError) {
                            $scope.errors = formatErrors(data.errors);
                        }
                        //setTimeout(refreshStatus, 3000);
                    }, function (resp) {
                        if ($scope.progress === 'Failed') {
                            $scope.maxRetry = 0;
                            return;
                        }
                        $scope.maxRetry--;
                        if ($scope.maxRetry === 0) {
                            if ($scope.progress !== null) {
                                $scope.progress = 'Done';
                            }
                        } else {
                            //setTimeout(refreshStatus, 3000);
                        }
                    });
                };

                this.$onInit = function () {
                    $scope.maxRetry = 1;
                    $scope.collapsed = true;
                    $scope.progress = null;
                    $scope.version = "";
                    refreshStatus();
                };

                self.launchTests = function () {
                    $scope.maxRetry = 10;
                    $scope.progress = null;
                    //setTimeout(refreshStatus, 3000);
                    RestHttp.restPost('launchTests', { serverId: self.id }).then(function (data) {
                        if (!$scope.progress) {
                            $scope.errors = [{
                                scenario: 'stdout',
                                message: data.stdout
                            }, {
                                scenario: 'stderr',
                                message: data.stderr
                            }];
                        }
                    }, function (resp) {
                        Notification.error(
                            {message: 'Failed to send the command.' + ' (' + resp.statusText + ')'}
                        );
                    });
                };

                self.collapse = function () {
                    $scope.collapsed = !$scope.collapsed;
                };
            }],
        bindings: {
            channel: '@',
            id: '@',
            coldata: '='
        }
    });

},{}],14:[function(require,module,exports){
/*global angular*/
'use strict';

angular.module('test-launcher-global', []);

},{}],15:[function(require,module,exports){
'use strict';
require('./test-launcher.module.js');
require('./test-launcher.component.js');

},{"./test-launcher.component.js":16,"./test-launcher.module.js":17}],16:[function(require,module,exports){
/*global angular*/
'use strict';

var template = '' +
'<style>' +
'    .borderbot > div > div {' +
'        border-bottom: solid #ddd 1px;' +
'    }' +
'    .borderright {' +
'        border-right: solid #ddd 1px;' +
'    }' +
'    .left {' +
'        float: left;' +
'        padding: 8px;' +
'    }' +
'    .bigcell {' +
'        width: 80%;' +
'    }' +
'    .smallcell {' +
'        width: 20%;' +
'    }' +
'</style>' +
'<hr>' +
'<button style="margin-bottom: 5px" ng-disabled="maxRetry !== 0" class="btn btn-default btn-block" ng-click="$ctrl.launchTests()">Launch tests on this server</button>' +
'<div ng-if="progress && progress !== \'Failed\'">' +
'    <table class="table table-bordered table-hover">' +
'        <tr>' +
'            <th>Version</th>' +
'            <td>{{version}}</td>' +
'        </tr>' +
'        <tr>' +
'            <th>Progress</th>' +
'            <td>{{progress}}</td>' +
'        </tr>' +
'        <tr>' +
'            <th>Details</th>' +
'            <td style="padding: 0">' +
'                <div ng-class="{\'borderbot\': !$last}" ng-repeat="detail in status[\'Details\']">' +
'                    <div ng-repeat="(key, value) in detail">' +
'                        <div class="left borderright bigcell">{{key}}</div>' +
'                        <div class="left smallcell">{{value}}</div>' +
'                    </div>' +
'                </div>' +
'            </td>' +
'        </tr>' +
'        <tr>' +
'            <th>Errors</th>' +
'            <td ng-if="!hasError">No errors.</td>' +
'            <td ng-if="hasError" style="padding: 0px">' +
'                <table class="table table-bordered table-hover" style="margin-bottom: 0px">' +
'                    <thead>' +
'                        <tr>' +
'                            <th>Scenario</th>' +
'                            <th>Player</th>' +
'                            <th>Message</th>' +
'                        </tr>' +
'                    </thead>' +
'                    <tbody>' +
'                        <tr ng-repeat="error in errors">' +
'                            <td>{{error.scenario}}</td>' +
'                            <td>{{error.player}}</td>' +
'                            <td>{{error.message}}</td>' +
'                        </tr>' +
'                    </tbody>' +
'                </table>' +
'            </td>' +
'        </tr>' +
'    </table>' +
'</div>' +
'<div ng-if="progress === \'Failed\'">' +
'    <div>Progress: {{progress}}</div>' +
'    <div ng-repeat="error in errors">' +
'        <div>{{error.scenario}}: {{error.message}}</div>' +
'    </div>' +
'</div>' +
'';


//noinspection JSUnresolvedFunction
angular.module('test-launcher').
    component('testLauncher', {
        template: template,
        controller: ['$scope', '$stateParams', 'Notification', 'RestHttp',
            function ($scope, $stateParams, Notification, RestHttp) {
                var formatErrors = function (errors) {
                    var errorArray = errors.split('\r');
                    var resArray = [];
                    var i, len;
                    var message, scenario, player, index;
                    for (i = 0, len = errorArray.length; i < len; i = i + 1) {
                        message = errorArray[i];
                        if (message !== '') { // skip empty lines
                            scenario = '';
                            player = '';
                            if (message.substring(0, 3) === '[Sc') {
                                index = message.indexOf(']') + 1;
                                scenario = message.substring(0, index);
                                scenario = scenario.substring(11, scenario.length - 1);
                                message = message.substring(index);
                                while (message.charAt(0) === ' ') {
                                    message = message.substr(1);
                                }
                            }
                            if (message.substring(0, 3) === '[Pl') {
                                index = message.indexOf(']') + 1;
                                player = message.substring(0, index);
                                player = player.substring(9, player.length - 1);
                                message = message.substring(index);
                                while (message.charAt(0) === ' ') {
                                    message = message.substr(1);
                                }
                            }
                            resArray.push({
                                message: message,
                                player: player,
                                scenario: scenario
                            });
                        }
                    }
                    return resArray;
                };

                var refreshStatus = function () {
                    RestHttp.restPost('getTestStatus', { serverId: $stateParams.id }).then(function (data) {
                        $scope.maxRetry = 1;
                        $scope.version = data.version;
                        $scope.progress = data.progress;
                        $scope.status = JSON.parse(data.status);
                        $scope.hasError = (data.errors !== 'No errors.');
                        if ($scope.hasError) {
                            $scope.errors = formatErrors(data.errors);
                        }
                        //setTimeout(refreshStatus, 3000);
                    }, function (resp) {
                        if ($scope.progress === 'Failed') {
                            $scope.maxRetry = 0;
                            return;
                        }
                        $scope.maxRetry--;
                        if ($scope.maxRetry === 0) {
                            if ($scope.progress !== null) {
                                $scope.progress = 'Done';
                            }
                            $scope.status = null;
                        } else {
                            //setTimeout(refreshStatus, 3000);
                        }
                    });
                };

                this.$onInit = function() {
                    $scope.maxRetry = 1;
                    $scope.progress = null;
                    $scope.version = "";
                    refreshStatus();
                };

                var self = this;

                self.launchTests = function() {
                    $scope.maxRetry = 10;
                    $scope.progress = null;
                    //setTimeout(refreshStatus, 3000);
                    RestHttp.restPost('launchTests', { serverId: $stateParams.id }).then(function (data) {
                        if (!$scope.errors) {
                            $scope.errors = [];
                        }
                        $scope.errors.push({ scenario: 'stdout', message: data.stdout });
                        $scope.errors.push({ scenario: 'stderr', message: data.stderr });
                    }, function (resp) {
                        Notification.error(
                            {message: 'Failed to send the command.' + ' (' + resp.statusText + ')'}
                        );
                    });
                };
            }],
        bindings: {
            channel: '@'
        }
    });

},{}],17:[function(require,module,exports){
/*global angular*/
'use strict';

angular.module('test-launcher', []);

},{}],18:[function(require,module,exports){
'use strict';
require('./upload-archive.module.js');
require('./upload-archive.component.js');

},{"./upload-archive.component.js":19,"./upload-archive.module.js":20}],19:[function(require,module,exports){
/*global angular*/
'use strict';

var template = '' +
'<hr>' +
'<label class="btn btn-default btn-file btn-block" ng-show="progress === 0">' +
'    Choose an archive <input fileread="fileread" type="file" accept=".tar.gz,.zip" class="hidden">' +
'</label>' +
'<div ng-show="fileread">' +
'    Selected: {{fileread.name}}' +
'</div>' +
'<div ng-show="progress !== 0">' +
'    Sending: {{progress}}%' +
'</div>' +
'<div style="margin-top: 3px;" ng-show="fileread" ng-disabled="progress !== 0">' +
'    <button class="btn btn-default btn-block" ng-click="$ctrl.sendFile()">Send</button>' +
'</div>' +
'';


//noinspection JSUnresolvedFunction
angular.module('upload-archive').
    component('uploadArchive', {
        template: template,
        controller: ['$scope', '$window', 'Notification',
            function ($scope, $window, Notification) {
                var self = this;

                var uploadFile = function (file, onProgress, cb) {
                    var xhr = new XMLHttpRequest();

                    xhr.open('POST', './rest/uploadArchive', true);

                    xhr.timeout = 15 * 60 * 1000; // 15 minutes
                    xhr.onload = function () {
                        if (xhr.readyState !== 4) {
                            return;
                        }
                        cb(xhr);
                    };
                    xhr.upload.onprogress = function (e) {
                        var percent;
                        if (e.lengthComputable) {
                            if (onProgress) {
                                percent = (e.loaded * 100) / e.total;
                                onProgress(percent);
                            }
                        }
                    };

                    var formData = new FormData();
                    formData.append('files', file, file.name);
                    xhr.send(formData);
                };

                this.sendFile = function () {
                    uploadFile($scope.fileread, function (percent) {
                        console.log(percent);
                        $scope.progress = parseInt(percent, 10);
                        $scope.$digest();
                    }, function (xhr) {
                        if (xhr.status !== 200) {
                            Notification.error({
                                message: 'Failed to upload the archive (' + xhr.responseText + ')'
                            });
                        } else {
                            Notification.success({
                                message: 'Archive uploaded successfully'
                            });
                            $window.location.reload();
                        }
                        $scope.fileread = null;
                        $scope.progress = 0;
                    });
                };

                this.$onInit = function () {
                    $scope.progress = 0;
                };
            }],
        bindings: {
            channel: '@'
        }
    })
    .directive("fileread", [function () {
        return {
            scope: {
                fileread: "="
            },
            link: function (scope, element, attributes) {
                element.bind("change", function (changeEvent) {
                    scope.$parent.fileread = changeEvent.target.files[0];
                    scope.$parent.$apply();
                });
            }
        };
    }]);

},{}],20:[function(require,module,exports){
/*global angular*/
'use strict';

angular.module('upload-archive', []);

},{}]},{},[1]);
