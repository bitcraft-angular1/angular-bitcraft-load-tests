module.exports = {
    sbm_monitoringResults: [{
        'time': {
            field: '_id',
            type: 'text'
        },
        'description': {
            field: 'description',
            type: 'text'
        }
    }, {

    }, {
        sort: true,
        edit: true,
        detailedView: true
    }, {
        component: 'testBasicResult',
        anchorToTop: false,
        detailedView: true
    }]
};