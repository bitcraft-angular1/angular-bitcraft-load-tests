module.exports = {
    sbm_clients: [{
        'address': {
            field: 'address',
            type: 'text'
        },
        'port': {
            field: 'port',
            type: 'number'
        },
        'test settings': {
            field: 'configuration',
            type: 'customConfigs'
        },
        'version': {
            field: 'version',
            type: 'availableVersion'
        },
        'status': {
            field: 'test-launcher-global',
            type: 'component'
        },
        'kill': {
            field: 'test-kill-global',
            type: 'component'
        }
    }, {
        'address': {
            field: 'address',
            type: 'text'
        },
        'port': {
            field: 'port',
            type: 'number'
        },
        'test settings': {
            field: 'configuration',
            type: 'customConfigs'
        },
        'version': {
            field: 'version',
            type: 'availableVersion'
        }
    }, {
        detailedTitle: 'Test client',
        sort: true,
        edit: true,
        delete: true,
        detailedView: true,
        add: {_id: "", address: "", port: 0, configuration: "Solo dungeon"}
    }, {
        component: 'testLauncher',
        anchorToTop: false,
        detailedView: true
    }, {
        component: 'testLaunchAll',
        anchorToTop: false,
        globalView: true
    }, {
        component: 'uploadArchive',
        anchorToTop: false,
        detailedView: true,
        globalView: true
    }, {
        component: 'editTextFile',
        anchorToTop: false,
        detailedView: true
    }]
};