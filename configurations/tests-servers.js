module.exports = {
    sbm_hosts: [{
        'localAddress': {
            field: 'localAddress',
            type: 'text'
        },
        'dns': {
            field: 'dns',
            type: 'text'
        },
        'category': {
            field: 'category',
            type: 'text'
        },
        'load': {
            field: 'bitcraft-hardware-monitoring',
            type: 'component'
        }
    }, {
        'address': {
            field: 'localAddress',
            type: 'text'
        },
        'dns': {
            field: 'dns',
            type: 'text'
        },
        'category': {
            field: 'category',
            type: 'text'
        }
    }, {
        detailedTitle: 'Test server',
        sort: true,
        edit: true,
        delete: true,
        detailedView: true
    }]
};