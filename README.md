# Components library for angular #

Current version : 1.0.0.

This module contains a collection of components to make load tests tabs inside an admintool.
It is completely dependent on db-edition, and assume a sentinel to be available on the server-nodes.
This sentinel must have a plugin to send the state of the machine.

## setup ##

You must have `bower` and `browserify` installed.
Run the command `bower install` in order to install the bower component `angular-utils`
Run the bash script `build.sh`

## plugins integration ##

You should reexport the laposte plugins used here by proxying them.

## configurations integration ##

This is not supposed to be used as is.
It is only given as a sample to build your configuration for db-edition.
