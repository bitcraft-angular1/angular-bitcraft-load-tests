/**
 * Created by richard on 15/02/17.
 */
'use strict';

require('./load-tests.module.js');
require('./test-basic-result');
require('./test-kill-global');
require('./test-launch-all');
require('./test-launcher');
require('./test-launcher-global');
require('./upload-archive');
