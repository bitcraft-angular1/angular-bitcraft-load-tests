/*global angular, $*/
'use strict';

//noinspection JSUnresolvedFunction
angular.module('test-launch-all').
    component('testLaunchAll', {
        templateUrl: 'js/db-edition/component/test-launch-all/test-launch-all.template.html',
        controller: ['$timeout', 'Notification',
            function ($timeout, Notification) {
                var self = this;

                this.startTests = function () {
                    $timeout(function () {
                        $('test-launcher-global button').click();
                    });
                };

                this.killTests = function () {
                    $timeout(function () {
                        $('test-kill-global button').click();
                    });
                };
            }],
        bindings: {
            channel: '@'
        }
    });
