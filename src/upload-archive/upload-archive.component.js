/*global angular*/
'use strict';

//noinspection JSUnresolvedFunction
angular.module('upload-archive').
    component('uploadArchive', {
        templateUrl: 'js/db-edition/component/upload-archive/upload-archive.template.html',
        controller: ['$scope', '$window', 'Notification',
            function ($scope, $window, Notification) {
                var self = this;

                var uploadFile = function (file, onProgress, cb) {
                    var xhr = new XMLHttpRequest();

                    xhr.open('POST', './rest/uploadArchive', true);

                    xhr.timeout = 15 * 60 * 1000; // 15 minutes
                    xhr.onload = function () {
                        if (xhr.readyState !== 4) {
                            return;
                        }
                        cb(xhr);
                    };
                    xhr.upload.onprogress = function (e) {
                        var percent;
                        if (e.lengthComputable) {
                            if (onProgress) {
                                percent = (e.loaded * 100) / e.total;
                                onProgress(percent);
                            }
                        }
                    };

                    var formData = new FormData();
                    formData.append('files', file, file.name);
                    xhr.send(formData);
                };

                this.sendFile = function () {
                    uploadFile($scope.fileread, function (percent) {
                        console.log(percent);
                        $scope.progress = parseInt(percent, 10);
                        $scope.$digest();
                    }, function (xhr) {
                        if (xhr.status !== 200) {
                            Notification.error({
                                message: 'Failed to upload the archive (' + xhr.responseText + ')'
                            });
                        } else {
                            Notification.success({
                                message: 'Archive uploaded successfully'
                            });
                            $window.location.reload();
                        }
                        $scope.fileread = null;
                        $scope.progress = 0;
                    });
                };

                this.$onInit = function () {
                    $scope.progress = 0;
                };
            }],
        bindings: {
            channel: '@'
        }
    })
    .directive("fileread", [function () {
        return {
            scope: {
                fileread: "="
            },
            link: function (scope, element, attributes) {
                element.bind("change", function (changeEvent) {
                    scope.$parent.fileread = changeEvent.target.files[0];
                    scope.$parent.$apply();
                });
            }
        };
    }]);
