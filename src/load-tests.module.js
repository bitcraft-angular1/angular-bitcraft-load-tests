/*global angular*/
'use strict';

angular.module('bitcraft-load-tests', [
    'test-basic-result',
    'test-kill-global',
    'test-launch-all',
    'test-launcher',
    'test-launcher-global',
    'upload-archive'
]);
