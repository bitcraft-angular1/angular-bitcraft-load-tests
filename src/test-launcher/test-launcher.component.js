/*global angular*/
'use strict';

//noinspection JSUnresolvedFunction
angular.module('test-launcher').
    component('testLauncher', {
        templateUrl: 'js/db-edition/component/test-launcher/test-launcher.template.html',
        controller: ['$scope', '$stateParams', 'Notification', 'RestHttp',
            function ($scope, $stateParams, Notification, RestHttp) {
                var formatErrors = function (errors) {
                    var errorArray = errors.split('\r');
                    var resArray = [];
                    var i, len;
                    var message, scenario, player, index;
                    for (i = 0, len = errorArray.length; i < len; i = i + 1) {
                        message = errorArray[i];
                        if (message !== '') { // skip empty lines
                            scenario = '';
                            player = '';
                            if (message.substring(0, 3) === '[Sc') {
                                index = message.indexOf(']') + 1;
                                scenario = message.substring(0, index);
                                scenario = scenario.substring(11, scenario.length - 1);
                                message = message.substring(index);
                                while (message.charAt(0) === ' ') {
                                    message = message.substr(1);
                                }
                            }
                            if (message.substring(0, 3) === '[Pl') {
                                index = message.indexOf(']') + 1;
                                player = message.substring(0, index);
                                player = player.substring(9, player.length - 1);
                                message = message.substring(index);
                                while (message.charAt(0) === ' ') {
                                    message = message.substr(1);
                                }
                            }
                            resArray.push({
                                message: message,
                                player: player,
                                scenario: scenario
                            });
                        }
                    }
                    return resArray;
                };

                var refreshStatus = function () {
                    RestHttp.restPost('getTestStatus', { serverId: $stateParams.id }).then(function (data) {
                        $scope.maxRetry = 1;
                        $scope.version = data.version;
                        $scope.progress = data.progress;
                        $scope.status = JSON.parse(data.status);
                        $scope.hasError = (data.errors !== 'No errors.');
                        if ($scope.hasError) {
                            $scope.errors = formatErrors(data.errors);
                        }
                        //setTimeout(refreshStatus, 3000);
                    }, function (resp) {
                        if ($scope.progress === 'Failed') {
                            $scope.maxRetry = 0;
                            return;
                        }
                        $scope.maxRetry--;
                        if ($scope.maxRetry === 0) {
                            if ($scope.progress !== null) {
                                $scope.progress = 'Done';
                            }
                            $scope.status = null;
                        } else {
                            //setTimeout(refreshStatus, 3000);
                        }
                    });
                };

                this.$onInit = function() {
                    $scope.maxRetry = 1;
                    $scope.progress = null;
                    $scope.version = "";
                    refreshStatus();
                };

                var self = this;

                self.launchTests = function() {
                    $scope.maxRetry = 10;
                    $scope.progress = null;
                    //setTimeout(refreshStatus, 3000);
                    RestHttp.restPost('launchTests', { serverId: $stateParams.id }).then(function (data) {
                        if (!$scope.errors) {
                            $scope.errors = [];
                        }
                        $scope.errors.push({ scenario: 'stdout', message: data.stdout });
                        $scope.errors.push({ scenario: 'stderr', message: data.stderr });
                    }, function (resp) {
                        Notification.error(
                            {message: 'Failed to send the command.' + ' (' + resp.statusText + ')'}
                        );
                    });
                };
            }],
        bindings: {
            channel: '@'
        }
    });
