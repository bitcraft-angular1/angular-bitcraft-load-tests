/*global angular*/
'use strict';

//noinspection JSUnresolvedFunction
angular.module('test-kill-global').
    component('testKillGlobal', {
        templateUrl: 'js/db-edition/component/test-kill-global/test-kill-global.template.html',
        controller: ['$scope', '$stateParams', 'Notification', 'RestHttp',
            function ($scope, $stateParams, Notification, RestHttp) {
                var self = this;

                self.killTest = function () {
                    RestHttp.restPost('killTest', { serverId: self.id }).then(function () {
                        Notification.success(
                            {message: 'Sent kill command.'}
                        );
                    }, function () {
                        Notification.error(
                            {message: 'Failed to send the command.' + ' (' + resp.statusText + ')'}
                        );
                    });
                };
            }],
        bindings: {
            channel: '@',
            id: '@'
        }
    });
