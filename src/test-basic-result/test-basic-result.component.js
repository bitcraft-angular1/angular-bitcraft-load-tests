/*global angular*/
'use strict';

//noinspection JSUnresolvedFunction
angular.module('test-basic-result').
    component('testBasicResult', {
        templateUrl: 'js/db-edition/component/test-basic-result/test-basic-result.template.html',
        controller: ['$scope', '$stateParams', 'Notification', 'RestHttp',
            function ($scope, $stateParams, Notification, RestHttp) {
                this.$onInit = function () {
                    $scope.hello = 'hello';
                    $scope.lines = [];
                    $scope.headHostnames = [];
                    $scope.headTitle = [];
                    RestHttp.restGet('getTestResult', {id: $stateParams.id}).then(function (data) {
                        console.log(data);
                        var key;
                        var keys = [];
                        var startedAt = data._id;
                        var i, j;
                        var done = false;

                        for (key in data) {
                            if (data.hasOwnProperty(key) && key !== '_id') {
                                keys.push(key);
                                $scope.headHostnames.push(key);
                                $scope.headTitle.push('cpu');
                                $scope.headTitle.push('memory');
                                $scope.headTitle.push('disk');

                                if (!done) {
                                    done = true;
                                    for (i = 0; i < data[key].length; i = i + 1, startedAt = startedAt + 60 * 1000) {
                                        $scope.lines.push({header: moment(new Date(startedAt)).format('MM/DD/YYYY hh:mm a'), data: []});
                                    }
                                }
                            }
                        }

                        for (i = 0; i < data[keys[0]].length; i = i + 1) {
                            for (j = 0; j < keys.length; j = j + 1) {
                                $scope.lines[i].data.push(data[keys[j]][i]['/hw/cpu']);
                                $scope.lines[i].data.push(data[keys[j]][i]['/hw/memory']);
                                $scope.lines[i].data.push(data[keys[j]][i]['/hw/disk']);
                            }
                        }

                        $scope.hello = 'hell done';
                    }, function (resp) {
                        Notification.error({
                            message: 'Failed to get the test results (' + resp.statusText + ')'
                        });
                    });
                };
            }],
        bindings: {
            channel: '@'
        }
    });
